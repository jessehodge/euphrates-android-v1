# Euphrates

Euphrates is the exciting new universal app that allows you to demand any service (e.g. jobs, projects, tasks) anytime, anywhere. Conversely, you can promote and offer your expertise. Whether you are in need of assistance, or one who can provide it, Euphrates is the real-time, highly flexible, and user-friendly application that's perfect for you.

## Integrations

The Euphrates backend is integrated with the following external libraries that may require new keys for you to use:

1. Firebase ~ For real time chat communication
2. Sinch ~ For real time VOIP communication

No personal data is used in the making of the calls or chat, simply uses the Devices ID and the user's id that is made when they register on the app.

## Installation
1. Clone this repository and import into **Android Studio**
```bash
git clone https://jessehodge@bitbucket.org/jessehodge/euphrates-android-v1.git
```
2. Then run it in an android emulator

![Screenshot](euphrates_image.png)

## MVP
-- Authentication
-- Search for Provider within 50 miles
-- Create Job Tags
-- Place in-screen call
-- Live chat
-- Payment of Escrow
-- Creation of invoice
-- Payment of Invoices