package corp.euphrates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import corp.euphrates.dao.UserCallback;
import corp.euphrates.dao.UserLocalStore;
import corp.euphrates.dao.UserRequest;
import corp.euphrates.models.Users;

public class RegistrationActivity extends AppCompatActivity {

    EditText mPassword, mEmail, mName;
    UserLocalStore localStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(corp.euphrates.R.layout.activity_registration);
        Toolbar toolbar = findViewById(corp.euphrates.R.id.toolbar);
        setSupportActionBar(toolbar);

        mName = findViewById(corp.euphrates.R.id.signup_input_name);
        mEmail = findViewById(corp.euphrates.R.id.signup_input_email);
        mPassword = findViewById(corp.euphrates.R.id.signup_input_password);
        mPassword.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister();
                return true;
            }
            return false;
        });

        Button mRegistrationButton = findViewById(corp.euphrates.R.id.registration_button);
        mRegistrationButton.setOnClickListener(v -> attemptRegister());
    }

    private void attemptRegister() {
        // Store values at the time of the login attempt.
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        String name = mName.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPassword.setError(getString(corp.euphrates.R.string.error_invalid_password));
            focusView = mPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmail.setError(getString(corp.euphrates.R.string.error_field_required));
            focusView = mEmail;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmail.setError(getString(corp.euphrates.R.string.error_invalid_email));
            focusView = mEmail;
            cancel = true;
        }

        if (TextUtils.isEmpty(name)) {
            mName.setError("This field is required");
            focusView = mName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            Users user = new Users(email, password, name);
            register(user);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private void register(Users user) {
        localStore = new UserLocalStore(this);
        UserRequest userRequest = new UserRequest(this);
        userRequest.registration(user, new UserCallback() {
            @Override
            public void login_done(Users user) {
            }

            @Override
            public void registration_done(Users user) {
                Context context = getApplicationContext();
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}

