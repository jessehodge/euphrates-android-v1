package corp.euphrates;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import corp.euphrates.models.Messages;

public class FirebaseChatViewHolder extends RecyclerView.ViewHolder {

    private final TextView message;

    FirebaseChatViewHolder(View itemView) {
        super(itemView);
        message = itemView.findViewById(corp.euphrates.R.id.message_text);
    }

    public void setMessage(Messages text, Boolean fromMe) {
        if (fromMe) {
            message.setText(text.message);
            message.setGravity(Gravity.START);
        } else {
            message.setText(text.message);
            message.setGravity(Gravity.END);
        }

    }
}
