package corp.euphrates.models;

public class Job {

    public String name, job_base_rate, job_unit_rate, unit, number_of_units;
    public int provider_id;

    public Job(String name, String job_base_rate, String unit, String job_unit_rate, int provider_id, String number_of_units) {
        this.name = name;
        this.job_base_rate = job_base_rate;
        this.unit = unit;
        this.job_unit_rate = job_unit_rate;
        this.provider_id = provider_id;
        this.number_of_units = number_of_units;
    }
    public Job(String name, String job_base_rate, String unit, String job_unit_rate, int provider_id) {
        this.name = name;
        this.job_base_rate = job_base_rate;
        this.unit = unit;
        this.job_unit_rate = job_unit_rate;
        this.provider_id = provider_id;
    }

    public Job(String name, String job_base_rate, String unit, String job_unit_rate) {
        this.name = name;
        this.job_base_rate = job_base_rate;
        this.job_unit_rate = job_unit_rate;
        this.unit = unit;
    }

    public Job() {};

    public Job(int provider_id) {
        this.provider_id = provider_id;
    }
}
