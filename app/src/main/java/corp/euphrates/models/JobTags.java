package corp.euphrates.models;

public class JobTags {
    public String name, baserate, unitrate, unit;
    public int provider_id;

    public JobTags(String name, String baserate, String unit, String unitrate, int provider_id) {
        this.name = name;
        this.baserate = baserate;
        this.unit = unit;
        this.unitrate = unitrate;
        this.provider_id = provider_id;
    }

    public JobTags(String name) {
        this.name = name;
    }
}
