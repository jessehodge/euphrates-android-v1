package corp.euphrates.models;

public class Users {
    public String email, password, token, name, longi, lati;
    public Boolean loggedIn;
    public int user_id;

    public Users(String email, String password, String token, Boolean loggedIn, int user_id) {
        this.email = email;
        this.password = password;
        this.token = token;
        this.loggedIn = loggedIn;
        this.user_id = user_id;
    }

    public Users(String email, String password, int user_id) {
        this.email = email;
        this.password = password;
        this.user_id = user_id;
    }

    public Users(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public Users(String email, String password) {
        this.email = email;
        this.password = password;
    }
}