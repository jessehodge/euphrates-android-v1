package corp.euphrates.models;

import java.util.ArrayList;

public class Messages {
    public String idSender, idReceiver, message;
    public long timestamp;

    public Messages(String idSender, String idReceiver, String text, long timestamp) {
        this.idSender = idSender;
        this.idReceiver = idReceiver;
        this.message = text;
        this.timestamp = timestamp;
    }

    public Messages() { }

    public String getMessage() {
        return message;
    }

    public static ArrayList<Messages> CreateChat() {
        return new ArrayList<>();
    }
}
