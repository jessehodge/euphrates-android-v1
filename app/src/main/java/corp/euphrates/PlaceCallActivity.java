package corp.euphrates;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;
import com.sinch.android.rtc.calling.CallListener;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import corp.euphrates.dao.JobCallback;
import corp.euphrates.dao.JobRequest;
import corp.euphrates.dao.PaymentRequest;
import corp.euphrates.dao.UserLocalStore;
import corp.euphrates.fragments.JobNegotiationFragment;
import corp.euphrates.fragments.consumer_frags.ConsumerHiredProviderFragment;
import corp.euphrates.fragments.consumer_frags.ConsumerJobInProgressFragment;
import corp.euphrates.fragments.provider_frags.ProviderJobDoneFragment;
import corp.euphrates.fragments.provider_frags.ProviderJobInProgressFragment;
import corp.euphrates.models.Job;
import corp.euphrates.models.Messages;

public class PlaceCallActivity extends AppCompatActivity implements
        JobNegotiationFragment.OnFragmentInteractionListener,
        ProviderJobInProgressFragment.OnFragmentInteractionListener,
        ProviderJobDoneFragment.OnFragmentInteractionListener,
        ConsumerHiredProviderFragment.OnFragmentInteractionListener,
        ConsumerJobInProgressFragment.OnFragmentInteractionListener {

    private Call call;
    private SinchClient sinchClient;
    private String consumerId, roomId, jobId, providerId;
    private EditText message_input;
    private int provider_id;
    private Job job_info;
    private DatabaseReference db = FirebaseDatabase.getInstance().getReference();
    private FirebaseRecyclerAdapter<Messages, FirebaseChatViewHolder> adapter;

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(corp.euphrates.R.layout.activity_place_call);
        final UserLocalStore localStore = new UserLocalStore(this);
        String method = getIntent().getStringExtra("method");
        if (getIntent().getStringExtra("consumer_id") == null) {
            consumerId = localStore.getConsumerId();
        } else {
            consumerId = getIntent().getStringExtra("consumer_id");
            localStore.saveConsumerId(consumerId);
        }
        if (getIntent().getStringExtra("provider_id") == null) {
            providerId = localStore.getProviderId();
        } else {
            providerId = getIntent().getStringExtra("provider_id");
            localStore.saveProviderId(providerId);
        }
        if (getIntent().getStringExtra("job_id") == null) {
            jobId = localStore.getJobId();
        } else {
            jobId = getIntent().getStringExtra("job_id");
            localStore.saveJobId(jobId);
        }
        String consumerName = getIntent().getStringExtra("consumer_name");
        if (getIntent().getStringExtra("room_id") == null) {
            roomId = localStore.getRoomId();
        } else {
            roomId = getIntent().getStringExtra("room_id");
            localStore.saveRoomId(roomId);
        }

        jobInformation();

        sinchClient = Sinch.getSinchClientBuilder()
                .context(this)
                .userId(String.valueOf(localStore.getUserId()))
                .applicationKey("0704ade9-cd1d-470f-8c26-14fd3640b6cd")
                .applicationSecret("9ZekZJPU/kqUN3bot8kVNw==")
                .environmentHost("sandbox.sinch.com")
                .build();

        sinchClient.setSupportCalling(true);
        sinchClient.startListeningOnActiveConnection();
        sinchClient.start();
        sinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

        // RecyclerView settings and configuration for the chat
        final RecyclerView mRecyclerView = findViewById(corp.euphrates.R.id.chat_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        Query chatRoomQuery = db.child("chat_rooms").child(roomId).limitToLast(20);
        final FirebaseRecyclerOptions<Messages> options = new FirebaseRecyclerOptions.Builder<Messages>()
                .setQuery(chatRoomQuery, Messages.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<Messages, FirebaseChatViewHolder>(options) {
            @NonNull
            @Override
            public FirebaseChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(corp.euphrates.R.layout.message, parent, false);

                return new FirebaseChatViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull FirebaseChatViewHolder holder, int position, @NonNull Messages model) {
                if (model.idSender.equals(String.valueOf(localStore.getUserId()))) {
                    holder.setMessage(model, true);
                } else {
                    holder.setMessage(model, false);
                }

            }
        };
        mRecyclerView.setAdapter(adapter);

        // Instantiates the Fragment in the Activity
        FrameLayout call_container = findViewById(corp.euphrates.R.id.chat_and_call_fragments);
        if (call_container != null) {
            if (savedInstanceState != null) {
                return;
            }
            JobNegotiationFragment jobNegotiationFragment = new JobNegotiationFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(corp.euphrates.R.id.chat_and_call_fragments, jobNegotiationFragment).commit();
        }
        // Adds settings to when you send a message in the chat.
        final Long tsLong = System.currentTimeMillis() / 1000;
        message_input = findViewById(corp.euphrates.R.id.send_message_input);

        findViewById(corp.euphrates.R.id.send_message_button).setOnClickListener(v -> {
            postMessage(String.valueOf(localStore.getUserId()), consumerId, message_input.getText().toString(), tsLong);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        });

        switch (method == null ? "" : method) {
            case "to_provider": {
                localStore.setRole("provider");
                localStore.setName(consumerName);
                provider_id = localStore.getUserId();
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("A Job is ready for you!");
                alertDialogBuilder.setPositiveButton("Accept", (dialog, which) -> notifyConsumer());
                alertDialogBuilder.setNegativeButton("Reject", (dialog, which) -> {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                break;
            }
            case "": {
                localStore.setRole("consumer");
                localStore.saveConsumerId(String.valueOf(localStore.getUserId()));
                localStore.setName(consumerName);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setMessage("A Provider has accepted your Job Request!");
                alertDialogBuilder.setPositiveButton("Ok", (dialog, which) -> {
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                break;
            }
            case "base_rate_changed": {
                Toast toast = Toast.makeText(getApplicationContext(), "Base Rate Updated!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
                break;
            }
            case "unit_rate_changed": {
                Toast toast = Toast.makeText(getApplicationContext(), "Unit Rate Updated!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
                break;
            }
            case "final_invoice": {
                Intent intent = new Intent(getApplicationContext(), ConsumerFinalInvoiceActivity.class);
                intent.putExtra("job_id", jobId);
                startActivity(intent);
            }
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    public void jobInformation() {
        getIntent().getStringExtra("job_tag");
        JobRequest jobRequest = new JobRequest(this);
        jobRequest.get(jobId, new JobCallback() {
            @Override
            public void editJobAddProvider(Job job) {

            }

            @Override
            public void receivedJobInfo(Job job) {
                if (job != null) {
                    TextView baseRate = findViewById(corp.euphrates.R.id.base_rate_input);
                    TextView unitRate = findViewById(corp.euphrates.R.id.unit_rate_input);
                    TextView unitLabel = findViewById(corp.euphrates.R.id.invoice_unit_label);
                    if (unitLabel != null) {
                        unitLabel.setText(job.unit);
                    }
                    baseRate.setText(String.format("$%s", job.job_base_rate));
                    unitRate.setText(String.format("$%s", job.job_unit_rate));
                    job_info = job;
                }
            }

            @Override
            public void editJobBaseRate(Job job) {

            }

            @Override
            public void editJobUnitRate(Job job) {

            }
        });
    }

    // Provider creates his Invoice to send to the Consumer
    public void invoiceCreation() {
        EditText number_of_units = findViewById(corp.euphrates.R.id.how_many_units);
        int final_rate = Integer.parseInt(number_of_units.getText().toString());
        PaymentRequest paymentRequest = new PaymentRequest(this);
        paymentRequest.postFinalPayment(final_rate, jobId, token -> {
        });
        Toast toast = Toast.makeText(getApplicationContext(), "Thank you for submitting your invoice!", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    // Fragment Navigation
    public void fragmentJobInProgressNav(String role) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (role.equals("provider")) {
            ProviderJobInProgressFragment providerJobInProgressFragment = new ProviderJobInProgressFragment();
            transaction.replace(corp.euphrates.R.id.chat_and_call_fragments, providerJobInProgressFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Alerting provider that you are ready for them to begin work!");
            alertDialogBuilder.setPositiveButton("Ok", (dialog, which) -> {
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);

            ConsumerHiredProviderFragment consumerHiredProviderFragment = new ConsumerHiredProviderFragment();
            transaction.replace(corp.euphrates.R.id.chat_and_call_fragments, consumerHiredProviderFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void fragmentJobDoneNav() {
        ProviderJobDoneFragment providerJobDoneFragment = new ProviderJobDoneFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(corp.euphrates.R.id.chat_and_call_fragments, providerJobDoneFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void fragmentConsumerJobInProgressNav() {
        ConsumerJobInProgressFragment consumerJobInProgressFragment = new ConsumerJobInProgressFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(corp.euphrates.R.id.chat_and_call_fragments, consumerJobInProgressFragment);
        transaction.addToBackStack(null);
        transaction.commit();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    // Sinch Functions
    public void callSinchUser(String role) {
        UserLocalStore localStore = new UserLocalStore(this);
        if (role.equals("provider")) {
            if (call == null) {
                if (consumerId == null) {
                    consumerId = localStore.getConsumerId();
                }
                call = sinchClient.getCallClient().callUser(consumerId);
                call.addCallListener(new SinchCallListener());
            } else {
                call.hangup();
            }
        } else {
            if (call == null) {
                if (providerId == null) {
                    providerId = localStore.getProviderId();
                }
                call = sinchClient.getCallClient().callUser(providerId);
                call.addCallListener(new SinchCallListener());
            } else {
                call.hangup();
            }
        }
    }

    // Edit Rate Functions
    public void editUnitRate() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        alertDialogBuilder.setView(input);
        alertDialogBuilder.setTitle("Enter a new Unit Rate!");
        alertDialogBuilder.setPositiveButton("Update Unit Rate", (dialog, which) -> {
            Job job = new Job();
            final UserLocalStore localStore = new UserLocalStore(PlaceCallActivity.this);
            jobId = localStore.getJobId();
            job.job_unit_rate = input.getText().toString();
            String email = localStore.getUserEmail();
            String consumer_id = localStore.getConsumerId();
            String provider_id = localStore.getProviderId();
            JobRequest jobRequest = new JobRequest(getApplicationContext());
            jobRequest.updateUnitRate(job, jobId, email, consumer_id, provider_id, new JobCallback() {
                @Override
                public void editJobAddProvider(Job job) {

                }

                @Override
                public void receivedJobInfo(Job job) {

                }

                @Override
                public void editJobBaseRate(Job job) {

                }

                @Override
                public void editJobUnitRate(Job job) {
                    TextView unit_rate = findViewById(corp.euphrates.R.id.unit_rate_input);
                    if (job != null) {
                        unit_rate.setText(String.format("$%s", job.job_unit_rate));
                    }
                    Toast toast = Toast.makeText(getApplicationContext(), "Unit Rate Updated!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                }
            });
        });
        alertDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> {
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public String getBaseRate() {
        if (job_info != null) {
            return job_info.job_base_rate;
        }
        return "";
    }

    public void editBaseRate() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        final EditText input = new EditText(this);
        alertDialogBuilder.setView(input);
        alertDialogBuilder.setTitle("Enter a new Base Rate!");
        alertDialogBuilder.setPositiveButton("Update Base Rate", (dialog, which) -> {
            Job job = new Job();
            final UserLocalStore localStore = new UserLocalStore(PlaceCallActivity.this);
            jobId = localStore.getJobId();
            job.job_base_rate = input.getText().toString();
            String email = localStore.getUserEmail();
            String consumer_id = localStore.getConsumerId();
            String provider_id = localStore.getProviderId();
            JobRequest jobRequest = new JobRequest(getApplicationContext());
            jobRequest.updateBaseRate(job, jobId, email, consumer_id, provider_id, new JobCallback() {
                @Override
                public void editJobAddProvider(Job job) {

                }

                @Override
                public void receivedJobInfo(Job job) {

                }

                @Override
                public void editJobBaseRate(Job job) {
                    TextView base_rate = findViewById(corp.euphrates.R.id.base_rate_input);
                    if (job != null) {
                        base_rate.setText(String.format("$%s", job.job_base_rate));
                    }
                    Toast toast = Toast.makeText(getApplicationContext(), "Base Rate Updated!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP, 0, 0);
                    toast.show();
                }

                @Override
                public void editJobUnitRate(Job job) {

                }
            });
        });
        alertDialogBuilder.setNegativeButton("Cancel", (dialog, which) -> {
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Submits the Escrow Payment
    public void onStripeSubmit() {
        CardMultilineWidget cardMultilineWidget = findViewById(corp.euphrates.R.id.card_input_widget);
        Card cardToSave = cardMultilineWidget.getCard();
        if (cardToSave == null) {
            Toast toast = Toast.makeText(getApplicationContext(), "Invalid Card Data", Toast.LENGTH_LONG);
            toast.show();
        } else {
            Stripe stripe = new Stripe(getApplicationContext(), "pk_test_7C5DeJUAz9A3ykGosLXyZalz");
            JobRequest jobRequest = new JobRequest(this);
            jobRequest.get(jobId, new JobCallback() {
                @Override
                public void editJobAddProvider(Job job) {

                }

                @Override
                public void receivedJobInfo(Job job) {
                    job_info = job;
                }

                @Override
                public void editJobBaseRate(Job job) {

                }

                @Override
                public void editJobUnitRate(Job job) {

                }
            });
            stripe.createToken(cardToSave, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Payment Error, Please try again.", Toast.LENGTH_LONG);
                    toast.show();
                }

                @Override
                public void onSuccess(Token token) {
                    String source = token.getId();
                    PaymentRequest paymentRequest = new PaymentRequest(getApplicationContext());
                    paymentRequest.postEscrowPayment(String.valueOf(job_info.job_base_rate), source, jobId, token1 -> fragmentConsumerJobInProgressNav());
                    Toast toast = Toast.makeText(getApplicationContext(), "Payment Successful!", Toast.LENGTH_LONG);
                    toast.show();
                }
            });
        }
    }

    // Notifies Consumer that a Provider has accepted the job
    private void notifyConsumer() {
        JobRequest jobRequest = new JobRequest(this);
        Job job;
        job = new Job(provider_id);
        jobRequest.updateProviderId(job, jobId, new JobCallback() {
            @Override
            public void editJobAddProvider(Job job) {
                TextView base_rate = findViewById(corp.euphrates.R.id.base_rate_input);
                TextView unit_rate = findViewById(corp.euphrates.R.id.unit_rate_input);
                base_rate.setText(String.format("$%s", job.job_base_rate));
                unit_rate.setText(String.format("$%s", job.job_unit_rate));
            }

            @Override
            public void receivedJobInfo(Job job) {

            }

            @Override
            public void editJobBaseRate(Job job) {

            }

            @Override
            public void editJobUnitRate(Job job) {

            }
        });
    }

    private void postMessage(String sender, String receiver, String message, long timestamp) {
        Messages messages = new Messages(sender, receiver, message, timestamp);
        String key = db.child("chat_rooms").child(roomId).push().getKey();
        Map<String, Object> childUpdates = new HashMap<>();
        if (!message.equals("")) {
            childUpdates.put("/chat_rooms/" + roomId + "/" + key, messages);
        }
        db.updateChildren(childUpdates);
        message_input.setText("");
    }

    private class SinchCallListener implements CallListener {
        TextView call_status_text_view;

        @Override
        public void onCallEnded(Call endedCall) {
            call = null;
            SinchError a = endedCall.getDetails().getError();
            call_status_text_view = findViewById(corp.euphrates.R.id.call_status_text_view);
            if (call_status_text_view != null) {
                call_status_text_view.setText("Call");
                call_status_text_view.setText("");
            }
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
        }

        @Override
        public void onCallEstablished(Call establishedCall) {
            call_status_text_view = findViewById(corp.euphrates.R.id.call_status_text_view);
            if (call_status_text_view != null) {
                call_status_text_view.setText("connected");
            }
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
        }

        @Override
        public void onCallProgressing(Call progressingCall) {
            call_status_text_view = findViewById(corp.euphrates.R.id.call_status_text_view);
            if (call_status_text_view != null) {
                call_status_text_view.setText("ringing");
            }
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
        }
    }

    private class SinchCallClientListener implements CallClientListener {
        TextView call_status_text_view;

        @Override
        public void onIncomingCall(CallClient callClient, Call incomingCall) {
            call_status_text_view = findViewById(corp.euphrates.R.id.call_status_text_view);
            call = incomingCall;
            Toast.makeText(getApplicationContext(), "incoming call", Toast.LENGTH_SHORT).show();
            call.answer();
            call.addCallListener(new SinchCallListener());
            if (call_status_text_view != null) {
                call_status_text_view.setText("Hang Up");
            }
        }
    }


}
