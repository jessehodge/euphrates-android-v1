package corp.euphrates.dao;

public interface PaymentCallback {
    void received_token(String token);
}
