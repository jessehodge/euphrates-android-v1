package corp.euphrates.dao;

import corp.euphrates.models.Job;

public interface JobCallback {
    void editJobAddProvider(Job job);
    void receivedJobInfo(Job job);
    void editJobBaseRate(Job job);
    void editJobUnitRate(Job job);
}
