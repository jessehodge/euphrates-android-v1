package corp.euphrates.dao;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class PaymentRequest {

    public PaymentRequest(Context context) {
    }

    public void postEscrowPayment(String base_rate, String source, String job_id, PaymentCallback paymentCallback) {
        new postEscrowPayment(base_rate, source, job_id, paymentCallback).execute();
    }

    public void postFinalPayment(int final_rate, String job_id, PaymentCallback paymentCallback) {
        new postFinalPayment(final_rate, job_id, paymentCallback).execute();
    }

    public void postFinalInvoicePayment(String final_rate, String source, PaymentCallback paymentCallback) {
        new postFinalInvoicePayment(final_rate, source, paymentCallback).execute();
    }

    static class postFinalInvoicePayment extends AsyncTask<Void, Void, String> {
        JSONObject paymentObject;
        PaymentCallback paymentCallback;
        String source, final_rate;

        postFinalInvoicePayment(String final_rate, String source, PaymentCallback paymentCallback) {
            this.final_rate = final_rate;
            this.source = source;
            this.paymentCallback = paymentCallback;
        }

        @Override
        protected String doInBackground(Void... params) {
            Map<String, String> PaymentData = new HashMap<>();
            PaymentData.put("charge", final_rate);
            PaymentData.put("source", source);
            paymentObject = new JSONObject(PaymentData);
            String inputLine;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.final_invoice_payment_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.paymentObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(paymentObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        source = jObject.getString("token");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return source;
        }

        @Override
        protected void onPostExecute(String source) {
            super.onPostExecute(source);
        }
    }

    static class postEscrowPayment extends AsyncTask<Void, Void, String> {
        JSONObject paymentObject;
        PaymentCallback paymentCallback;
        String source, base_rate, job_id;

        postEscrowPayment(String base_rate, String source, String job_id, PaymentCallback paymentCallback) {
            this.paymentCallback = paymentCallback;
            this.base_rate = base_rate;
            this.source = source;
            this.job_id = job_id;
        }

        @Override
        protected String doInBackground(Void... params) {
            Map<String, String> PaymentData = new HashMap<>();
            PaymentData.put("charge", base_rate);
            PaymentData.put("source", source);
            PaymentData.put("job_id", job_id);
            paymentObject = new JSONObject(PaymentData);
            String inputLine;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.escrow_payment_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.paymentObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(paymentObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        source = jObject.getString("token");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return source;
        }

        @Override
        protected void onPostExecute(String token) {
            super.onPostExecute(token);
            paymentCallback.received_token(token);
        }
    }

    static class postFinalPayment extends AsyncTask<Void, Void, String> {
        int final_rate;
        String job_id;
        PaymentCallback paymentCallback;
        JSONObject paymentObject;

        postFinalPayment(int final_rate, String job_id, PaymentCallback paymentCallback) {
            this.final_rate = final_rate;
            this.job_id = job_id;
            this.paymentCallback = paymentCallback;
        }

        @Override
        protected String doInBackground(Void... params) {
            Map<String, String> PaymentData = new HashMap<>();
            PaymentData.put("number_of_units", String.valueOf(final_rate));
            paymentObject = new JSONObject(PaymentData);
            String inputLine;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.provider_job_update_url + job_id + '/');
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PATCH");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.paymentObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(paymentObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        job_id = jObject.getString("token");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return job_id;
        }

        @Override
        protected void onPostExecute(String source) {
            super.onPostExecute(source);
        }
    }

}
