package corp.euphrates.dao;

import com.google.firebase.auth.FirebaseUser;

public interface FBUserCallback {
    void createFBUser(FirebaseUser firebaseUser);
}

