package corp.euphrates.dao;

import corp.euphrates.models.Job;

public interface ConsumerJobsListCallback {
    void receievedJobList(Job job);
}
