package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.internal.firebase_auth.zzao;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseUserMetadata;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FBUserRequest {
    private static ProgressDialog progressDialog;

    public FBUserRequest(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please Wait...");
    }

    public void createFBUser(FirebaseUser firebaseUser, String token, int user_id, FBUserCallback fbUserCallback) {
        progressDialog.show();
        new createFBUser(firebaseUser, token, user_id, fbUserCallback).execute();
    }

    static class createFBUser extends AsyncTask<Void, Void, FirebaseUser> {
        JSONObject fbObject;
        FirebaseUser user;
        FBUserCallback fbUserCallback;
        int user_id;
        String token;

        createFBUser(FirebaseUser firebaseUser, String token, int user_id, FBUserCallback fbUserCallback) {
            this.user = firebaseUser;
            this.user_id = user_id;
            this.fbUserCallback = fbUserCallback;
            this.token = token;
        }

        @Override
        protected FirebaseUser doInBackground(Void... params) {
            Map<String, String> FBUserData = new HashMap<>();
            token = token.substring(7);
            FBUserData.put("registration_id", FirebaseInstanceId.getInstance().getToken());
            FBUserData.put("type", "android");
            FBUserData.put("name", FirebaseInstanceId.getInstance().getId());
            FBUserData.put("device_id", FirebaseInstanceId.getInstance().getId());
            FBUserData.put("user", String.valueOf(user_id));
            FBUserData.put("active", "true");
            fbObject = new JSONObject(FBUserData);
            String inputLine;
            FirebaseUser returnedUser = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.device_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Authorization", "Token " + token);

                if (this.fbObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(fbObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        String token = "Bearer " + jObject.getString("token");
                        returnedUser = new FirebaseUser() {
                            @NonNull
                            @Override
                            public String getUid() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public String getProviderId() {
                                return null;
                            }

                            @Override
                            public boolean isAnonymous() {
                                return false;
                            }

                            @Nullable
                            @Override
                            public List<String> getProviders() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public List<? extends UserInfo> getProviderData() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public FirebaseUser zza(@NonNull List<? extends UserInfo> list) {
                                return null;
                            }

                            @Override
                            public FirebaseUser zzn() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public FirebaseApp zzo() {
                                return null;
                            }

                            @Nullable
                            @Override
                            public String getDisplayName() {
                                return null;
                            }

                            @Nullable
                            @Override
                            public Uri getPhotoUrl() {
                                return null;
                            }

                            @Nullable
                            @Override
                            public String getEmail() {
                                return null;
                            }

                            @Nullable
                            @Override
                            public String getPhoneNumber() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public zzao zzp() {
                                return null;
                            }

                            @Override
                            public void zza(@NonNull zzao zzao) {

                            }

                            @NonNull
                            @Override
                            public String zzq() {
                                return null;
                            }

                            @NonNull
                            @Override
                            public String zzr() {
                                return null;
                            }

                            @Nullable
                            @Override
                            public FirebaseUserMetadata getMetadata() {
                                return null;
                            }

                            @Override
                            public void writeToParcel(Parcel dest, int flags) {

                            }

                            @Override
                            public boolean isEmailVerified() {
                                return false;
                            }
                        };
                    }
                } else {
                    return new FirebaseUser() {
                        @NonNull
                        @Override
                        public String getUid() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public String getProviderId() {
                            return null;
                        }

                        @Override
                        public boolean isAnonymous() {
                            return false;
                        }

                        @Nullable
                        @Override
                        public List<String> getProviders() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public List<? extends UserInfo> getProviderData() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public FirebaseUser zza(@NonNull List<? extends UserInfo> list) {
                            return null;
                        }

                        @Override
                        public FirebaseUser zzn() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public FirebaseApp zzo() {
                            return null;
                        }

                        @Nullable
                        @Override
                        public String getDisplayName() {
                            return null;
                        }

                        @Nullable
                        @Override
                        public Uri getPhotoUrl() {
                            return null;
                        }

                        @Nullable
                        @Override
                        public String getEmail() {
                            return null;
                        }

                        @Nullable
                        @Override
                        public String getPhoneNumber() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public zzao zzp() {
                            return null;
                        }

                        @Override
                        public void zza(@NonNull zzao zzao) {

                        }

                        @NonNull
                        @Override
                        public String zzq() {
                            return null;
                        }

                        @NonNull
                        @Override
                        public String zzr() {
                            return null;
                        }

                        @Nullable
                        @Override
                        public FirebaseUserMetadata getMetadata() {
                            return null;
                        }

                        @Override
                        public void writeToParcel(Parcel dest, int flags) {

                        }

                        @Override
                        public boolean isEmailVerified() {
                            return false;
                        }
                    };
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnedUser;
        }

        @Override
        protected void onPostExecute(FirebaseUser firebaseUser) {
            super.onPostExecute(firebaseUser);
            progressDialog.dismiss();
            fbUserCallback.createFBUser(firebaseUser);
        }
    }

}

