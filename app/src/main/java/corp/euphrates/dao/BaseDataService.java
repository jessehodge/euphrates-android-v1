package corp.euphrates.dao;

import android.app.Application;


public class BaseDataService extends Application {
    //    public static String base_url = "http://157.230.170.162/api/v1/";
    public static String base_url = "http://10.0.2.2:8000/api/v1/";

    public static String device_url = "devices/";

    public static String job_url = "job/";
    public static String job_tag_url = "job_tag/";
    public static String consumer_jobs_list_url = "consumer_jobs/";
    public static String base_rate_url = "base_rate/";
    public static String unit_rate_url = "unit_rate/";

    public static String provider_job_update_url = "provider_job_update/";
    public static String final_invoice_payment_url = "final_invoice_payment/";

    public static String search_url = "search/";

    public static String escrow_payment_url = "escrow_payment/";

    public static String login_url = "auth/login/";
    public static String registration_url = "auth/register/";

    public static int READ_TIMEOUT = 15000;
    public static int CONNECTION_TIMEOUT = 15000;

}


