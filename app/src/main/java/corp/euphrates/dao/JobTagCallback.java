package corp.euphrates.dao;

import corp.euphrates.models.JobTags;

public interface JobTagCallback {
    void job_tag_creation_done(JobTags jobTags);
}
