package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import corp.euphrates.models.JobTags;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SearchRequest {

    private static ProgressDialog progressDialog;

    public SearchRequest(Context context) {
//        progressDialog = new ProgressDialog(context);
//        progressDialog.setCancelable(false);
//        progressDialog.setTitle("Processing...");
//        progressDialog.setMessage("Please Wait...");
    }

    public void get(JobTags jobTags, int consumer_id, String key, SearchCallback searchCallback) {
//        progressDialog.show();
        new get(jobTags, consumer_id, key, searchCallback).execute();
    }

    static class get extends AsyncTask<Void, Void, Boolean> {
        JSONObject searchObject;
        JobTags jobTags;
        SearchCallback searchCallback;
        Boolean success;
        String key;
        int consumer_id;

        get(JobTags jobTags, int consumer_id, String key, SearchCallback searchCallback) {
            this.jobTags = jobTags;
            this.searchCallback = searchCallback;
            this.consumer_id = consumer_id;
            this.key = key;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Map<String, String> SearchData = new HashMap<>();
            searchObject = new JSONObject(SearchData);
            success = false;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.search_url + jobTags.name + '/' + consumer_id + '/' + key + '/');
                // HttpURLConnection defaults to a GET request
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    success = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
//            progressDialog.dismiss();
            searchCallback.search_done(success);
        }
    }
}