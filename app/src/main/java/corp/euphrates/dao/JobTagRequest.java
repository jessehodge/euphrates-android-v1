package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import corp.euphrates.models.JobTags;

public class JobTagRequest {

    private static ProgressDialog progressDialog;

    public JobTagRequest(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please Wait...");
    }

    public void create(JobTags jobTags, JobTagCallback jobTagCallback) {
        progressDialog.show();
        new create(jobTags, jobTagCallback).execute();
    }

    static class create extends AsyncTask<Void, Void, JobTags> {
        JSONObject jobTagObject;
        JobTags jobTags;
        JobTagCallback jobTagCallback;

        create(JobTags jobTags, JobTagCallback jobTagCallback) {
            this.jobTags = jobTags;
            this.jobTagCallback = jobTagCallback;
        }

        @Override
        protected JobTags doInBackground(Void... params) {
            Map<String, String> JobTagData = new HashMap<>();
            JobTagData.put("name", jobTags.name);
            JobTagData.put("job_base_rate", jobTags.baserate);
            JobTagData.put("unit", jobTags.unit);
            JobTagData.put("job_unit_rate", jobTags.unitrate);
            String user_id = String.valueOf(jobTags.provider_id);
            JobTagData.put("providers", user_id);
            jobTagObject = new JSONObject(JobTagData);
            String inputLine;
            JobTags jobTag = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.job_tag_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.jobTagObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(jobTagObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        jobTag = new JobTags("", "", "", "", 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return jobTag;
        }

        @Override
        protected void onPostExecute(JobTags returnedJobTag) {
            super.onPostExecute(returnedJobTag);
            progressDialog.dismiss();
            jobTagCallback.job_tag_creation_done(returnedJobTag);
        }
    }
}