package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import corp.euphrates.models.Job;

public class ConsumerJobsListRequest {


    private static ProgressDialog progressDialog;

    public ConsumerJobsListRequest(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please Wait...");
    }

    public void get(String consumer_id, ConsumerJobsListCallback consumerJobsListCallback) {
        progressDialog.show();
        new ConsumerJobsListRequest.get(consumer_id, consumerJobsListCallback).execute();
    }

    static class get extends AsyncTask<Void, Void, Job> {

        JSONObject jobObject;
        ConsumerJobsListCallback consumerJobsListCallback;
        String consumer_id;

        get(String consumer_id, ConsumerJobsListCallback consumerJobsListCallback) {
            this.consumerJobsListCallback = consumerJobsListCallback;
            this.consumer_id = consumer_id;
        }

        @Override
        protected Job doInBackground(Void... params) {
            Map<String, String> SearchData = new HashMap<>();
            jobObject = new JSONObject(SearchData);
            String inputLine;
            Job returnedJob = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.consumer_jobs_list_url + consumer_id);
                // HttpURLConnection defaults to a GET request
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        returnedJob = new Job("", jObject.getString("job_base_rate"), "", jObject.getString("job_unit_rate"), 0, jObject.getString("number_of_units"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnedJob;
        }

        @Override
        protected void onPostExecute(Job returnedJob) {
            super.onPostExecute(returnedJob);
            progressDialog.dismiss();
            consumerJobsListCallback.receievedJobList(returnedJob);
        }
    }
}
