package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import corp.euphrates.models.Job;

public class JobRequest {

    private static ProgressDialog progressDialog;

    public JobRequest(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please Wait...");
    }

    public void updateBaseRate(Job job, String job_id, String email, String consumer_id, String provider_id, JobCallback jobCallback) {
        new JobRequest.updateBaseRate(job, job_id, email, consumer_id, provider_id, jobCallback).execute();
    }

    public void updateUnitRate(Job job, String job_id, String email, String consumer_id, String provider_id, JobCallback jobCallback) {
        new JobRequest.updateUnitRate(job, job_id, email, consumer_id, provider_id, jobCallback).execute();
    }

    public void updateProviderId(Job job, String job_id, JobCallback jobCallback) {
        progressDialog.show();
        new JobRequest.updateProviderId(job, job_id, jobCallback).execute();
    }

    public void get(String job_id, JobCallback jobCallback) {
        progressDialog.show();
        new JobRequest.get(job_id, jobCallback).execute();
    }

    static class updateUnitRate extends AsyncTask<Void, Void, Job> {
        final Job job;
        final JobCallback jobCallback;
        final String job_id, email, consumer_id, provider_id;
        JSONObject jobObject;

        updateUnitRate(Job job, String job_id, String email, String consumer_id, String provider_id, JobCallback jobCallback) {
            this.job = job;
            this.jobCallback = jobCallback;
            this.job_id = job_id;
            this.email = email;
            this.consumer_id = consumer_id;
            this.provider_id = provider_id;
        }

        @Override
        protected Job doInBackground(Void... params) {
            Map<String, String> JobData = new HashMap<>();
            JobData.put("job_unit_rate", String.valueOf(job.job_unit_rate));
            JobData.put("user_email", email);
            JobData.put("job_id", job_id);
            JobData.put("consumer_id", consumer_id);
            JobData.put("provider_id", provider_id);
            jobObject = new JSONObject(JobData);
            String inputLine;
            Job job = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.unit_rate_url + job_id + "/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("PATCH");
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.jobObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(jobObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        job = new Job("", "", "", jObject.getString("job_unit_rate"), 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return job;
        }

        @Override
        protected void onPostExecute(Job job) {
            super.onPostExecute(job);
            jobCallback.editJobUnitRate(job);
        }
    }

    static class updateBaseRate extends AsyncTask<Void, Void, Job> {
        final Job job;
        final JobCallback jobCallback;
        final String job_id, email, consumer_id, provider_id;
        JSONObject jobObject;

        updateBaseRate(Job job, String job_id, String email, String consumer_id, String provider_id, JobCallback jobCallback) {
            this.job = job;
            this.jobCallback = jobCallback;
            this.job_id = job_id;
            this.email = email;
            this.consumer_id = consumer_id;
            this.provider_id = provider_id;
        }

        @Override
        protected Job doInBackground(Void... params) {
            Map<String, String> JobData = new HashMap<>();
            JobData.put("job_base_rate", String.valueOf(job.job_base_rate));
            JobData.put("user_email", email);
            JobData.put("consumer_id", consumer_id);
            JobData.put("provider_id", provider_id);
            JobData.put("job_id", job_id);
            jobObject = new JSONObject(JobData);
            String inputLine;
            Job job = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.base_rate_url + job_id + "/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("PATCH");
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.jobObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(jobObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        job = new Job("", jObject.getString("job_base_rate"), "", "", 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return job;
        }

        @Override
        protected void onPostExecute(Job job) {
            super.onPostExecute(job);
            jobCallback.editJobBaseRate(job);
        }
    }

    static class updateProviderId extends AsyncTask<Void, Void, Job> {
        final Job job;
        final JobCallback jobCallback;
        final String job_id;
        JSONObject jobObject;

        updateProviderId(Job job, String job_id, JobCallback jobCallback) {
            this.job = job;
            this.jobCallback = jobCallback;
            this.job_id = job_id;
        }

        @Override
        protected Job doInBackground(Void... params) {
            Map<String, String> JobData = new HashMap<>();
            JobData.put("providers", String.valueOf(job.provider_id));
            jobObject = new JSONObject(JobData);
            String inputLine;
            Job job = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.job_url + job_id + "/");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("PUT");
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.jobObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(jobObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        job = new Job("", jObject.getString("job_base_rate"), "", jObject.getString("job_unit_rate"), 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return job;
        }

        @Override
        protected void onPostExecute(Job returnedJob) {
            super.onPostExecute(returnedJob);
            progressDialog.dismiss();
            jobCallback.editJobAddProvider(returnedJob);
        }
    }

    static class get extends AsyncTask<Void, Void, Job> {

        JSONObject jobObject;
        JobCallback jobCallback;
        String job_id;

        get(String job_id, JobCallback jobCallback) {
            this.jobCallback = jobCallback;
            this.job_id = job_id;
        }

        @Override
        protected Job doInBackground(Void... params) {
            Map<String, String> SearchData = new HashMap<>();
            jobObject = new JSONObject(SearchData);
            String inputLine;
            Job returnedJob = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.job_url + job_id + '/');
                // HttpURLConnection defaults to a GET request
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        returnedJob = new Job("", jObject.getString("job_base_rate"), "", jObject.getString("job_unit_rate"), 0, jObject.getString("number_of_units"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnedJob;
        }

        @Override
        protected void onPostExecute(Job returnedJob) {
            super.onPostExecute(returnedJob);
            progressDialog.dismiss();
            jobCallback.receivedJobInfo(returnedJob);
        }
    }
}
