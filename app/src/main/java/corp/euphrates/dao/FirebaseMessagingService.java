package corp.euphrates.dao;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

import corp.euphrates.PlaceCallActivity;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (applicationInForeground()) {
            Intent intent = new Intent(getApplicationContext(), PlaceCallActivity.class);
            if (remoteMessage.getData().get("method") != null) {
                String method = remoteMessage.getData().get("method");
                intent.putExtra("method", method);
            }
            if (remoteMessage.getData().get("name") != null) {
                String name = remoteMessage.getData().get("name");
                intent.putExtra("name", name);
            }
            if (remoteMessage.getData().get("job_id") != null) {
                String job = remoteMessage.getData().get("job_id");
                intent.putExtra("job_id", job);
            }
            if (remoteMessage.getData().get("consumer_id") != null) {
                String consumer = remoteMessage.getData().get("consumer_id");
                intent.putExtra("consumer_id", consumer);
            }
            if (remoteMessage.getData().get("consumer_name") != null) {
                String consumer_name = remoteMessage.getData().get("consumer_name");
                intent.putExtra("consumer_name", consumer_name);
            }
            if (remoteMessage.getData().get("room_id") != null) {
                String room = remoteMessage.getData().get("room_id");
                intent.putExtra("room_id", room);
            }
            if (remoteMessage.getData().get("provider_id") != null) {
                String providerId = remoteMessage.getData().get("provider_id");
                intent.putExtra("provider_id", providerId);
            }
            if (remoteMessage.getData().get("job_base_rate") != null) {
                String jobTagBaseRate = remoteMessage.getData().get("job_base_rate");
                intent.putExtra("job_base_rate", jobTagBaseRate);
            }
            if (remoteMessage.getData().get("job_unit_rate") != null) {
                String jobTagUnitRate = remoteMessage.getData().get("job_unit_rate");
                intent.putExtra("job_unit_rate", jobTagUnitRate);
            }
            startActivity(intent);
        }
    }

    private boolean applicationInForeground() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> services = activityManager.getRunningAppProcesses();
        boolean isActivityFound = false;

        if (services.get(0).processName
                .equalsIgnoreCase(getPackageName())) {
            isActivityFound = true;
        }

        return isActivityFound;
    }
}
