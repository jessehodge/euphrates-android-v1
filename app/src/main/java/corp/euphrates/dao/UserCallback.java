package corp.euphrates.dao;

import corp.euphrates.models.Users;

public interface UserCallback {
    void login_done(Users user);
    void registration_done(Users user);
}

