package corp.euphrates.dao;

import android.content.Context;
import android.content.SharedPreferences;

import corp.euphrates.models.Users;

public class UserLocalStore {
    private static final String spName = "UserDetails";
    private SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context) {
        userLocalDatabase = context.getSharedPreferences(spName, 0);
    }

    public void storeUserData(Users user) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("email", user.email);
        spEditor.putString("token", user.token);
        spEditor.putInt("user_id", user.user_id);
        spEditor.putBoolean("loggedIn", true);
        spEditor.apply();
    }

    public Users getLoggedInUser() {
        if (!userLocalDatabase.getBoolean("loggedIn", true)) {
            return null;
        } else {
            String token = userLocalDatabase.getString("token", "");
            String email = userLocalDatabase.getString("email", "");

            return new Users(email, "", token, true, 0);
        }
    }

    public void logOut() {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.remove("token");
        spEditor.apply();
    }

    public void saveRoomId(String roomId) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("roomId", roomId);
        spEditor.apply();
    }

    public void saveJobId(String jobId) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("job_id", jobId);
        spEditor.apply();
    }

    public void saveConsumerId(String consumer_id) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("consumer_id", consumer_id);
        spEditor.apply();
    }

    public void saveProviderId(String provider_id) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("provider_id", provider_id);
        spEditor.apply();
    }

    public String getProviderId() {
        return userLocalDatabase.getString("provider_id", "");
    }

    public String getConsumerId() {
        return userLocalDatabase.getString("consumer_id", "");
    }

    public String getUserEmail() {
        return userLocalDatabase.getString("email", "");
    }

    public String getJobId() {
        return userLocalDatabase.getString("job_id", "");
    }

    public String getBraintreeClientToken() {
        return userLocalDatabase.getString("braintree_client_token", "");
    }

    public void setBraintreeClientToken(String token) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("braintree_client_token", token);
        spEditor.apply();
    }

    public String getRole() {
        return userLocalDatabase.getString("role", "");
    }

    public void setRole(String role) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("role", role);
        spEditor.apply();
    }

    public String getName() {
        return userLocalDatabase.getString("name", "");
    }

    public void setName(String name) {
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("name", name);
        spEditor.apply();
    }

    public String getRoomId() {
        return userLocalDatabase.getString("roomId", "");
    }

    public int getUserId() {
        return userLocalDatabase.getInt("user_id", 0);
    }
}

