package corp.euphrates.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import corp.euphrates.models.Users;

public class UserRequest {

    private static ProgressDialog progressDialog;

    public UserRequest(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please Wait...");
    }

    public void login(Users user, UserCallback userCallback) {
        progressDialog.show();
        new login(user, userCallback).execute();
    }

    public void registration(Users user, UserCallback userCallback) {
        progressDialog.show();
        new registration(user, userCallback).execute();
    }

    private static class login extends AsyncTask<Void, Void, Users> {
        JSONObject userObject;
        Users user;
        UserCallback userCallback;

        login(Users user, UserCallback userCallback) {
            this.user = user;
            this.userCallback = userCallback;
        }

        @Override
        protected Users doInBackground(Void... params) {
            Map<String, String> UserData = new HashMap<>();
            UserData.put("email", user.email);
            UserData.put("password", user.password);
            UserData.put("longitude", user.longi);
            UserData.put("latitude", user.lati);
            userObject = new JSONObject(UserData);
            String inputLine;
            Users returnedUser = null;
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.login_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.userObject != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(userObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        String token = "Bearer " + jObject.getString("token");
                        returnedUser = new Users(user.email, "", token, true, jObject.getInt("id"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnedUser;
        }

        @Override
        protected void onPostExecute(Users returnedUser) {
            super.onPostExecute(returnedUser);
            progressDialog.dismiss();
            userCallback.login_done(returnedUser);
        }
    }

    private static class registration extends AsyncTask<Void, Void, Users> {
        JSONObject userObject;
        Users user;
        UserCallback userCallback;

        registration(Users user, UserCallback userCallback) {
            this.user = user;
            this.userCallback = userCallback;
        }

        @Override
        protected Users doInBackground(Void... params) {
            Map<String, String> UserData = new HashMap<>();
            UserData.put("email", user.email);
            UserData.put("password", user.password);
            UserData.put("name", user.name);
            String inputLine;
            Users returnedUser = null;
            userObject = new JSONObject(UserData);
            try {
                URL url = new URL(BaseDataService.base_url + BaseDataService.registration_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                // Sets it to POST
                connection.setDoOutput(true);
                connection.setReadTimeout(BaseDataService.READ_TIMEOUT);
                connection.setConnectTimeout(BaseDataService.CONNECTION_TIMEOUT);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                if (this.userObject != null) {
                    DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
                    writer.writeBytes(userObject.toString());
                    writer.flush();
                }

                int statusCode = connection.getResponseCode();

                if (statusCode == 200) {
                    InputStreamReader streamReader = new InputStreamReader(connection.getInputStream());

                    BufferedReader reader = new BufferedReader(streamReader);
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((inputLine = reader.readLine()) != null) {
                        stringBuilder.append(inputLine);
                    }
                    reader.close();
                    streamReader.close();
                    String result = stringBuilder.toString();
                    JSONObject jObject = new JSONObject(result);
                    if (jObject.length() != 0) {
                        Integer user_id = jObject.getJSONObject("user").getInt("id");
                        String token = "Bearer " + jObject.getString("token");
                        returnedUser = new Users(user.email, token, user_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return returnedUser;
        }

        @Override
        protected void onPostExecute(Users returnedUser) {
            super.onPostExecute(returnedUser);
            progressDialog.dismiss();
            userCallback.registration_done(returnedUser);
        }
    }
}


