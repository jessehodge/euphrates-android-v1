package corp.euphrates;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import corp.euphrates.dao.JobCallback;
import corp.euphrates.dao.JobRequest;
import corp.euphrates.dao.PaymentRequest;
import corp.euphrates.models.Job;

public class ConsumerFinalInvoiceActivity extends AppCompatActivity {

    private CardMultilineWidget cardMultilineWidget;
    private Float final_rate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(corp.euphrates.R.layout.activity_consumer_final_invoice);

        String job = getIntent().getStringExtra("job_id");
        JobRequest jobRequest = new JobRequest(this);
        jobRequest.get(job, new JobCallback() {
            @Override
            public void editJobAddProvider(Job job) {

            }

            @Override
            public void receivedJobInfo(Job job) {
                TextView unit_rate_due = findViewById(corp.euphrates.R.id.total_amount_due);
                Float final_total = Float.parseFloat(job.job_unit_rate) * Float.parseFloat(job.number_of_units);
                final_rate = final_total;
                unit_rate_due.setText(String.format("$%s", final_total.toString()));
            }

            @Override
            public void editJobBaseRate(Job job) {

            }

            @Override
            public void editJobUnitRate(Job job) {

            }
        });

        findViewById(corp.euphrates.R.id.submit_final_payment_button).setOnClickListener(v -> {
            cardMultilineWidget = findViewById(corp.euphrates.R.id.final_invoice_cc_info);
            Card cardToSave = cardMultilineWidget.getCard();

            if (cardToSave == null) {
                Toast toast = Toast.makeText(getApplicationContext(), "Invalid Card Data", Toast.LENGTH_LONG);
                toast.show();
            } else {
                Stripe stripe = new Stripe(getApplicationContext(), "pk_test_7C5DeJUAz9A3ykGosLXyZalz");

                stripe.createToken(cardToSave, new TokenCallback() {
                    @Override
                    public void onError(Exception error) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Payment Error, Please try again.", Toast.LENGTH_LONG);
                        toast.show();
                    }

                    @Override
                    public void onSuccess(Token token) {
                        String source = token.getId();
                        PaymentRequest paymentRequest = new PaymentRequest(getApplicationContext());
                        paymentRequest.postFinalInvoicePayment(final_rate.toString(), source, token1 -> {
                        });
                        Toast toast = Toast.makeText(getApplicationContext(), "Payment Successful!", Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });
    }
}
