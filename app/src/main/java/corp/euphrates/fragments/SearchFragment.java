package corp.euphrates.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import corp.euphrates.MainActivity;
import corp.euphrates.R;
import corp.euphrates.dao.SearchRequest;
import corp.euphrates.dao.UserLocalStore;
import corp.euphrates.models.JobTags;

public class SearchFragment extends android.support.v4.app.Fragment {

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView title = view.findViewById(R.id.advanced_search_options_text);

        Button mSearchButton = view.findViewById(R.id.search_button);
        mSearchButton.setOnClickListener(v -> attemptSearch());

        // Fonts
        Typeface typeface = getResources().getFont(R.font.lato_semibold);
        title.setTypeface(typeface);
    }

    private void attemptSearch() {
        MainActivity mainActivity = (MainActivity) this.getActivity();
        String searchText = mainActivity.mSearch.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            UserLocalStore localStore = new UserLocalStore(getContext());
            int consumer_id = localStore.getUserId();
            DatabaseReference db = FirebaseDatabase.getInstance().getReference();
            String key = db.child("chats").push().getKey();
            localStore.saveRoomId(key);
            JobTags jobTags = new JobTags(searchText);
            SearchRequest searchRequest = new SearchRequest(getContext());
            searchRequest.get(jobTags, consumer_id, key, success -> {
                Toast toast = Toast.makeText(getContext(), "Search Initialized!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP, 0, 0);
                toast.show();
            });
        }
    }
}
