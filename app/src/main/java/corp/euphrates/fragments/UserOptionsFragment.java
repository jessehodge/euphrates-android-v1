package corp.euphrates.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import corp.euphrates.LoginActivity;
import corp.euphrates.R;

public class UserOptionsFragment extends android.support.v4.app.Fragment {


    public UserOptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_options, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button mChangePassword = view.findViewById(R.id.change_password_button);
        Button mLogout = view.findViewById(R.id.logout_button);
        Typeface semibold_typeface = getResources().getFont(R.font.lato_semibold);

        mChangePassword.setTypeface(semibold_typeface);
        mLogout.setTypeface(semibold_typeface);

        mLogout.setOnClickListener(v -> {
            Context context = v.getContext();
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
        });

        mChangePassword.setOnClickListener(v -> {
        });
    }
}
