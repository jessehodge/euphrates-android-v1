package corp.euphrates.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import corp.euphrates.PlaceCallActivity;
import corp.euphrates.R;
import corp.euphrates.dao.UserLocalStore;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JobNegotiationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JobNegotiationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobNegotiationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    public JobNegotiationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JobNegotiationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobNegotiationFragment newInstance(String param1, String param2) {
        JobNegotiationFragment fragment = new JobNegotiationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_job_negotiation, container, false);
        // Inflate the layout for this fragment

        final UserLocalStore localStore = new UserLocalStore(getContext());
        final String role = localStore.getRole();
        TextView name = rootView.findViewById(R.id.providername);
        final PlaceCallActivity placeCallActivity = (PlaceCallActivity) getActivity();
        name.setText(localStore.getName());
        final ImageButton call_status_button = rootView.findViewById(R.id.call_status_button);

        call_status_button.setOnClickListener(v -> {
            placeCallActivity.callSinchUser(role);
            TextView textView = rootView.findViewById(R.id.call_status_text_view);
            textView.setText("Hang up");
            textView.setTextColor(0xFFFF0000);
        });
        rootView.findViewById(R.id.providername);
        rootView.findViewById(R.id.job_negotiation_navigation_button).setOnClickListener(
                v -> placeCallActivity.fragmentJobInProgressNav(role));
        rootView.findViewById(R.id.edit_unit_rate_button).setOnClickListener(v -> placeCallActivity.editUnitRate());
        rootView.findViewById(R.id.edit_base_rate_button).setOnClickListener(v -> placeCallActivity.editBaseRate());
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri textView);
    }
}
