package corp.euphrates.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import corp.euphrates.JobTagActivity;
import corp.euphrates.R;

public class ProviderOptionsFragment extends android.support.v4.app.Fragment {


    public ProviderOptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_provider_options, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button mRouteToJobTag = view.findViewById(R.id.create_job_tag_button);

        mRouteToJobTag.setOnClickListener(v -> routeToJobTagActivity());
    }

    private void routeToJobTagActivity() {
        Intent intent = new Intent(getActivity(), JobTagActivity.class);
        startActivity(intent);
    }
}
