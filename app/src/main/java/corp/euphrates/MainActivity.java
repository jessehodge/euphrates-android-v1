package corp.euphrates;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import corp.euphrates.dao.ConsumerJobsListRequest;
import corp.euphrates.dao.UserLocalStore;
import corp.euphrates.fragments.FavoritesFragment;
import corp.euphrates.fragments.ProviderOptionsFragment;
import corp.euphrates.fragments.SearchFragment;
import corp.euphrates.fragments.UserOptionsFragment;
import corp.euphrates.models.Users;

public class MainActivity extends AppCompatActivity {

    public EditText mSearch;
    private TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(corp.euphrates.R.layout.activity_main);

        Toolbar toolbar = findViewById(corp.euphrates.R.id.toolbar);
        setSupportActionBar(toolbar);

        UserLocalStore localStore = new UserLocalStore(this);
        Users user = localStore.getLoggedInUser();
        if (user.token.length() <= 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        ActionBar action = getSupportActionBar();
        action.setDisplayShowTitleEnabled(false);
        action.setDisplayShowCustomEnabled(true);
        action.setCustomView(corp.euphrates.R.layout.search_bar);

        mSearch = findViewById(corp.euphrates.R.id.edtSearch);
        Typeface typeface = getResources().getFont(corp.euphrates.R.font.lato_semibold);
        mSearch.setTypeface(typeface);

        ViewPager viewPager = findViewById(corp.euphrates.R.id.container);
        tabLayout = findViewById(corp.euphrates.R.id.tabs);
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        ConsumerJobsListRequest consumerJobsListRequest = new ConsumerJobsListRequest(this);
        consumerJobsListRequest.get(localStore.getConsumerId(), job -> {
            Bundle bundle = new Bundle();
            if (job != null) {
                bundle.putString("json", job.toString());
            }

            UserOptionsFragment userOptionsFragment = new UserOptionsFragment();
            userOptionsFragment.setArguments(bundle);
        });
    }

    private void setupTabIcons() {
        int[] tabIcons = {
                corp.euphrates.R.drawable.ic_user_icon,
                corp.euphrates.R.drawable.ic_gear_icon,
                corp.euphrates.R.drawable.ic_contacts_icon,
                corp.euphrates.R.drawable.ic_search_white,
        };
        tabLayout.getTabAt(0).setIcon(tabIcons[3]);
        tabLayout.getTabAt(1).setIcon(tabIcons[0]);
        tabLayout.getTabAt(2).setIcon(tabIcons[1]);
        tabLayout.getTabAt(3).setIcon(tabIcons[2]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new SearchFragment(), "Search Fragment");
        adapter.addFrag(new UserOptionsFragment(), "ONE");
        adapter.addFrag(new ProviderOptionsFragment(), "TWO");
        adapter.addFrag(new FavoritesFragment(), "THREE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return to display only the icon
            return null;
        }
    }
}

