package corp.euphrates;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import corp.euphrates.dao.JobTagRequest;
import corp.euphrates.dao.UserLocalStore;
import corp.euphrates.models.JobTags;

public class JobTagActivity extends AppCompatActivity {

    EditText mJobTagName;
    EditText mJobTagBaseRate;
    EditText mJobTagUnit;
    EditText mJobTagUnitRate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(corp.euphrates.R.layout.activity_job_tag);
        Toolbar toolbar = findViewById(corp.euphrates.R.id.toolbar);
        setSupportActionBar(toolbar);

        mJobTagName = findViewById(corp.euphrates.R.id.job_tag_name_input);
        mJobTagBaseRate = findViewById(corp.euphrates.R.id.job_tag_baserate_input);
        mJobTagUnit = findViewById(corp.euphrates.R.id.job_tag_unit_input);
        mJobTagUnitRate = findViewById(corp.euphrates.R.id.job_tag_unit_rate_input);

        Button mSubmitJobTag = findViewById(corp.euphrates.R.id.create_job_tag_submission);
        mSubmitJobTag.setOnClickListener(v -> attemptJobTagSubmission());
    }

    private void attemptJobTagSubmission() {
        String jobTagName = mJobTagName.getText().toString();
        String jobTagBaseRate = mJobTagBaseRate.getText().toString();
        String jobTagUnit = mJobTagUnit.getText().toString();
        String jobTagUnitRate = mJobTagUnitRate.getText().toString();
        UserLocalStore localStore = new UserLocalStore(this);
        int user_id = localStore.getUserId();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(jobTagName)) {
            mJobTagName.setError("This field is required.");
            focusView = mJobTagName;
            cancel = true;
        }

        if (TextUtils.isEmpty(jobTagBaseRate)) {
            mJobTagBaseRate.setError("This field is required.");
            focusView = mJobTagName;
            cancel = true;
        }

        if (TextUtils.isEmpty(jobTagUnit)) {
            mJobTagUnit.setError("This field is required.");
            focusView = mJobTagName;
            cancel = true;
        }

        if (TextUtils.isEmpty(jobTagUnitRate)) {
            mJobTagUnitRate.setError("This field is required.");
            focusView = mJobTagUnitRate;
            cancel = true;
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            JobTags jobTags = new JobTags(jobTagName, jobTagBaseRate, jobTagUnit, jobTagUnitRate, user_id);
            create(jobTags);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private void create(JobTags jobTags) {
        JobTagRequest jobTagRequest = new JobTagRequest(this);
        jobTagRequest.create(jobTags, jobTags1 -> createJobTag());
    }

    private void createJobTag() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}

